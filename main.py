from admin import Administrator
from database import Database


def main():
    # Create an instance of the Database class
    database = Database()

    # Create an instance of the Administrator class, passing the database instance as an argument
    admin = Administrator(database)

    admin.authorize()

    # Perform operations based on user input or triggering event
    while True:
        print("1. Add new item")
        print("2. Remove item")
        print("3. Add new category")
        print("4. Exit")

        choice = input("Enter your choice: ")

        if choice == "1":
            admin.add_item()
        elif choice == "2":
            admin.remove_item()
        elif choice == "3":
            admin.add_category()
        elif choice == "4":
            print("Exiting...")
            break
        else:
            print("Invalid choice. Please try again.")


if __name__ == "__main__":
    main()